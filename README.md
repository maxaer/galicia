Peter Kaminski pkaminsk@usc.edu 
Cole Brossart cbrossar@usc.edu 
Carl Eubanks ceubanks@usc.edu
Elizabeth Worstell worstell@usc.edu 

Maxaer

Requirements:

Java SE 7
Something to run a JAR file

How to play:

- Launch Jar file labled maxaer.jar
- Might say "maxaer.jar is from an unidentified developer. Are you sure you want to open it?"
- Click Open
